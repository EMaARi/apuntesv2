#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>

#define MAX 20

int main(int argc, char *argv[]) {

	char nombre[MAX],
		valor;
	int edad,
		contador = 0;

	printf("Intoduce tu nombre: ");
	scanf("%s", nombre);
	printf("Intoduce tu edad: ");
	scanf("%i", &edad);

	printf("Tu nombre es %s [%i años]\n", nombre, edad);

	printf("Entrando bucle\n");

	do {
		printf("Introduce la letra correcta: ");
		scanf("%s", valor);
		contador++;
		valor = tolower(valor);
		printf("Letra incorrecta [intento %i]\n", contador);
	} while (valor != 'g');

	return EXIT_SUCCESS;
}
